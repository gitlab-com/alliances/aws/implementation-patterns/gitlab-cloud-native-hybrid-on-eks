* Environment:                3k-quickstart-autoscale-arm-rds-cache
* Environment Version:        13.12.3-ee `9d9769ba2ad`
* Option:                     thistestrps
* Date:                       2021-07-23
* Run Time:                   1h 7m 20.19s (Start: 19:42:00 UTC, End: 20:49:20 UTC)
* GPT Version:                v2.8.0

❯ Overall Results Score: 99.46%

NAME                                                     | RPS  | RPS RESULT         | TTFB AVG   | TTFB P90              | REQ STATUS     | RESULT         
---------------------------------------------------------|------|--------------------|------------|-----------------------|----------------|----------------
api_v4_groups                                            | 60/s | 59.66/s (>48.00/s) | 65.99ms    | 73.00ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_groups_group                                      | 60/s | 45.13/s (>4.80/s)  | 1199.55ms  | 1700.05ms (<7505ms)   | 100.00% (>99%) | Passed
api_v4_groups_group_subgroups                            | 60/s | 59.7/s (>48.00/s)  | 71.07ms    | 81.62ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_groups_issues                                     | 60/s | 59.01/s (>14.40/s) | 222.77ms   | 305.72ms (<3505ms)    | 100.00% (>99%) | Passed
api_v4_groups_merge_requests                             | 60/s | 58.97/s (>14.40/s) | 209.75ms   | 309.24ms (<3505ms)    | 100.00% (>99%) | Passed
api_v4_groups_projects                                   | 60/s | 58.02/s (>24.00/s) | 665.31ms   | 1143.23ms (<3505ms)   | 100.00% (>99%) | Passed
api_v4_projects                                          | 60/s | 30.14/s (>7.20/s)  | 1828.88ms  | 3041.27ms (<7005ms)   | 100.00% (>99%) | Passed
api_v4_projects_deploy_keys                              | 60/s | 59.86/s (>48.00/s) | 34.80ms    | 41.81ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_issues                                   | 60/s | 59.37/s (>48.00/s) | 150.90ms   | 210.10ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_issues_issue                             | 60/s | 59.38/s (>48.00/s) | 147.97ms   | 189.68ms (<1505ms)    | 100.00% (>99%) | Passed
api_v4_projects_issues_search                            | 60/s | 42.35/s (>7.20/s)  | 1273.88ms  | 2454.81ms (<12005ms)  | 100.00% (>99%) | Passed
api_v4_projects_languages                                | 60/s | 59.84/s (>48.00/s) | 30.06ms    | 34.17ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_merge_requests                           | 60/s | 59.26/s (>28.80/s) | 145.83ms   | 179.93ms (<1005ms)    | 100.00% (>99%) | Passed
api_v4_projects_merge_requests_merge_request             | 60/s | 58.67/s (>24.00/s) | 182.00ms   | 236.38ms (<2755ms)    | 100.00% (>99%) | Passed
api_v4_projects_merge_requests_merge_request_changes     | 60/s | 49.76/s (>24.00/s) | 1076.05ms  | 1747.19ms (<3505ms)   | 100.00% (>99%) | Passed
api_v4_projects_merge_requests_merge_request_commits     | 60/s | 59.75/s (>48.00/s) | 47.72ms    | 54.24ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_merge_requests_merge_request_discussions | 60/s | 59.39/s (>48.00/s) | 124.39ms   | 152.44ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_project                                  | 60/s | 59.59/s (>48.00/s) | 86.83ms    | 100.12ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_project_pipelines                        | 60/s | 59.83/s (>48.00/s) | 40.45ms    | 46.11ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_project_pipelines_pipeline               | 60/s | 59.38/s (>48.00/s) | 51.64ms    | 57.35ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_project_services                         | 60/s | 59.88/s (>48.00/s) | 33.16ms    | 38.17ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_releases                                 | 60/s | 59.61/s (>48.00/s) | 54.88ms    | 63.24ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_repository_branches                      | 60/s | 59.85/s (>48.00/s) | 31.40ms    | 35.04ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_repository_branches_branch               | 60/s | 59.71/s (>48.00/s) | 59.82ms    | 67.13ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_repository_branches_search               | 60/s | 59.52/s (>14.40/s) | 31.79ms    | 34.76ms (<6005ms)     | 100.00% (>99%) | Passed
api_v4_projects_repository_commits                       | 60/s | 59.83/s (>48.00/s) | 51.93ms    | 56.98ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_repository_commits_commit                | 60/s | 59.55/s (>48.00/s) | 113.00ms   | 143.58ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_repository_commits_commit_diff           | 60/s | 59.53/s (>48.00/s) | 143.34ms   | 211.89ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_repository_compare                       | 60/s | 12.07/s (>4.80/s)  | 4604.14ms  | 6427.50ms (<8005ms)   | 100.00% (>99%) | Passed
api_v4_projects_repository_files_file                    | 60/s | 59.71/s (>48.00/s) | 73.82ms    | 76.00ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_repository_files_file_blame              | 60/s | 3.87/s (>0.48/s)   | 13925.46ms | 20092.01ms (<35005ms) | 100.00% (>99%) | Passed
api_v4_projects_repository_files_file_raw                | 60/s | 59.71/s (>48.00/s) | 66.54ms    | 71.98ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_repository_tags                          | 60/s | 34.43/s (>9.60/s)  | 1598.16ms  | 2787.17ms (<10005ms)  | 100.00% (>99%) | Passed
api_v4_projects_repository_tree                          | 60/s | 59.67/s (>48.00/s) | 67.46ms    | 75.94ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_user                                              | 60/s | 59.87/s (>48.00/s) | 31.09ms    | 35.90ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_users                                             | 60/s | 59.91/s (>48.00/s) | 36.87ms    | 44.00ms (<505ms)      | 100.00% (>99%) | Passed
git_ls_remote                                            | 6/s  | 6.01/s (>4.80/s)   | 36.71ms    | 40.20ms (<505ms)      | 100.00% (>99%) | Passed
git_pull                                                 | 6/s  | 6.01/s (>4.80/s)   | 49.82ms    | 62.55ms (<505ms)      | 100.00% (>99%) | Passed
git_push                                                 | 2/s  | 1.25/s (>0.96/s)   | 282.74ms   | 420.51ms (<1005ms)    | 100.00% (>99%) | Passed
web_group                                                | 6/s  | 6.01/s (>4.80/s)   | 90.95ms    | 111.54ms (<505ms)     | 100.00% (>99%) | Passed
web_group_issues                                         | 6/s  | 5.91/s (>4.80/s)   | 203.16ms   | 219.45ms (<505ms)     | 100.00% (>99%) | Passed
web_group_merge_requests                                 | 6/s  | 5.98/s (>4.80/s)   | 188.68ms   | 195.26ms (<505ms)     | 100.00% (>99%) | Passed
web_project                                              | 6/s  | 5.96/s (>4.80/s)   | 213.47ms   | 233.61ms (<505ms)     | 100.00% (>99%) | Passed
web_project_branches                                     | 6/s  | 5.71/s (>4.80/s)   | 545.77ms   | 613.95ms (<1005ms)    | 100.00% (>99%) | Passed
web_project_branches_search                              | 6/s  | 5.69/s (>4.80/s)   | 650.92ms   | 732.97ms (<1305ms)    | 100.00% (>99%) | Passed
web_project_commit                                       | 6/s  | 5.64/s (>0.96/s)   | 584.13ms   | 1629.89ms (<10005ms)  | 100.00% (>99%) | Passed
web_project_commits                                      | 6/s  | 5.87/s (>4.80/s)   | 374.59ms   | 411.71ms (<755ms)     | 100.00% (>99%) | Passed
web_project_file_blame                                   | 6/s  | 1.77/s (>0.05/s)   | 2935.08ms  | 3684.19ms (<7005ms)   | 100.00% (>99%) | Passed
web_project_file_rendered                                | 6/s  | 5.88/s (>2.88/s)   | 503.65ms   | 653.55ms (<3005ms)    | 100.00% (>99%) | Passed
web_project_file_source                                  | 6/s  | 5.72/s (>0.48/s)   | 561.16ms   | 802.40ms (<1705ms)    | 100.00% (>99%) | Passed
web_project_files                                        | 6/s  | 5.93/s (>4.80/s)   | 140.53ms   | 176.93ms (<805ms)     | 100.00% (>99%) | Passed
web_project_issue                                        | 6/s  | 5.97/s (>4.80/s)   | 225.97ms   | 654.70ms (<2005ms)    | 100.00% (>99%) | Passed
web_project_issues                                       | 6/s  | 5.94/s (>4.80/s)   | 223.74ms   | 242.64ms (<505ms)     | 100.00% (>99%) | Passed
web_project_issues_search                                | 6/s  | 5.94/s (>4.80/s)   | 234.79ms   | 246.85ms (<505ms)     | 100.00% (>99%) | Passed
web_project_merge_request                                | 6/s  | 5.67/s (>1.92/s)   | 870.55ms   | 3611.00ms (<7505ms)   | 100.00% (>99%) | Passed
web_project_merge_request_changes                        | 6/s  | 5.95/s (>4.80/s)   | 306.32ms   | 540.77ms (<1505ms)    | 100.00% (>99%) | Passed
web_project_merge_request_commits                        | 6/s  | 5.78/s (>2.88/s)   | 590.80ms   | 653.69ms (<1755ms)    | 100.00% (>99%) | Passed
web_project_merge_requests                               | 6/s  | 5.95/s (>4.80/s)   | 220.48ms   | 260.35ms (<505ms)     | 100.00% (>99%) | Passed
web_project_pipelines                                    | 6/s  | 5.92/s (>2.88/s)   | 250.54ms   | 374.33ms (<1005ms)    | 100.00% (>99%) | Passed
web_project_pipelines_pipeline                           | 6/s  | 5.95/s (>4.80/s)   | 412.97ms   | 931.20ms (<2505ms)    | 100.00% (>99%) | Passed
web_project_repository_compare                           | 6/s  | 1.42/s (>0.24/s)   | 3851.79ms  | 4325.90ms (<7505ms)   | 100.00% (>99%) | Passed
web_project_tags                                         | 6/s  | 5.72/s (>3.84/s)   | 566.61ms   | 621.96ms (<1505ms)    | 100.00% (>99%) | Passed
web_user                                                 | 6/s  | 5.97/s (>2.88/s)   | 153.51ms   | 232.45ms (<4005ms)    | 100.00% (>99%) | Passed
