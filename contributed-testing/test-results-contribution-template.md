
## Your Configuration:

### EKS Cluster
- EKS Cluster Target vCPUs and MBs Memory: `0 vCPUs, 0 GB`
- EKS Node Instance Type: 
- EKS Node Numbers

### Aurora RDS
- Aurora Target vCPUs and MBs Memory: `0 vCPUs, 0 GB`
- Aurora Instance Type (Graviton is OK): 

### Redis Elasticache
- Redis Target vCPUs and MBs Memory: `0 vCPUs, 0 GB`
- Redis Instance Type (Graviton is OK): 

### Gitaly Instance
- Gitaly Target vCPUs and MBs Memory: `0 vCPUs, 0 GB`
- Gitaly Instance Type: 

### Praefect Instance
- Praefect Target vCPUs and MBs Memory: `0 vCPUs, 0 GB`
- Praefect Instance Type: 

## Your Test Results

`paste the test results .txt file here`